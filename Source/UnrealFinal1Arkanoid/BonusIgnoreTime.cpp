// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusIgnoreTime.h"
#include "PlayerPawn.h"

void ABonusIgnoreTime::GiveBonus(AActor* bonusTarget)
{
	APlayerPawn* pawn = Cast<APlayerPawn>(bonusTarget);
	if (pawn) {
		pawn->AddIngnoreTimeBall();
	}
}
