// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractorInterface.h"
#include "GameFramework/Actor.h"
#include "BlockActor.generated.h"

class UStaticMeshComponent;
class USoundBase;
class IBonusInterface;
class AGameFieldACtor;

UENUM()
enum EBonusType {
	SPEEDUPBALL,
	SPEEDDOWNBALL,
	SPEEDUPPAWN,
	SPEEDDOWNPAWN,
	SIZEUPPAWN,
	SIZEDOWNPAWN,
	IGNOREBLOCK,
	INGNOREBLOCKTIME,
	BLOWUP,
	ADDBALL
};

UCLASS()
class UNREALFINAL1ARKANOID_API ABlockActor : public AActor, public IInteractorInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlockActor();

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* blockMeshComponent;

	UPROPERTY(EditDefaultsOnly)
		USoundBase* HitBallSound;

	UPROPERTY(EditDefaultsOnly)
		USoundBase* DeadSound;


	UPROPERTY(EditDefaultsOnly)
		int bonusPrec = 5;

	UPROPERTY(EditDefaultsOnly)
		UMaterial* BlockMaterialDestruction = nullptr;

	UPROPERTY(EditDefaultsOnly)
		int HP = 100;

	UPROPERTY(EditDefaultsOnly)
		int BallKick = 50;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		AGameFieldACtor* fieldOwner;

	UPROPERTY(EditDefaultsOnly)
		int blockScore = 100;

	UPROPERTY()
		AGameFieldACtor* Parent = nullptr;

	UFUNCTION()
	void HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
			AActor* otherActor,
			UPrimitiveComponent* otherComponent,
			int32 otherBodyIndex,
			bool bFromSweep,
			const FHitResult& sweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* otherActor, const FHitResult& hitResult) override;

};
