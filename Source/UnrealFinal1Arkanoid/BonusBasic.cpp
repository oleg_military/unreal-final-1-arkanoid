// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBasic.h"

// Sets default values
ABonusBasic::ABonusBasic()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ballMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ballMeshComponent"));
	ballMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ballMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void ABonusBasic::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABonusBasic::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorWorldOffset(movementDiraction * speedBall, true);

}

void ABonusBasic::GiveBonus(AActor* bonusTarget)
{
}

