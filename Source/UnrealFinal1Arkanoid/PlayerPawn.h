// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "InteractorInterface.h"
#include "BonusInterface.h"
#include "DataScore.h"
#include "PlayerPawn.generated.h"
class UStaticMeshComponent;
class UCapsuleComponent;
class ABallActor;

USTRUCT()
struct FActorSaveData
{
	GENERATED_BODY()

public:
	UPROPERTY()
		TArray<int32> ByteData;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnKickPlayerLives);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnScore);

UCLASS()

class UNREALFINAL1ARKANOID_API APlayerPawn : public APawn, public IInteractorInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

	UFUNCTION(BlueprintCallable, Category = "Pawn")
		void HandlerPlayerPawnMoveInpput(float value);

	UFUNCTION(BlueprintCallable, Category = "Pawn")
		void HandlerPlayerPushBallInpput();

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* PlatformMeshComponent;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UCapsuleComponent* BallsSpawnCapsule;

	UPROPERTY(EditDefaultsOnly)
		USoundBase* HitBallSound;

	UPROPERTY(EditDefaultsOnly)
		USoundBase* BonusSound;

	UPROPERTY(EditDefaultsOnly)
		USoundBase* HitBallDead;

	UPROPERTY(Category = Pawn, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UPawnMovementComponent* MovementComponent;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		TArray<ABallActor*> Balls;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		ABallActor* BallForSpawn;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		FVector DeltaMovement;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABallActor> BallActorClass;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UDataScore* DataScores;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		float MovementCoef = 1;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		float MovementSide = 0;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		float MovementSpeed = 400.f;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		int BallReserverCount = 1;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		int PlayerScore = 0;
	
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		bool bDoOnce = true;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		FString PlayerName = "No Name";

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		TArray<FDataScoreItem> PlayerRecordScore;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		int PlayerLives = 3;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		float BallsSpeed = 2;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		float PawnScale = 1;

	UPROPERTY(BlueprintAssignable)
		FOnKickPlayerLives OnKickPlayerLives;

	UPROPERTY(BlueprintAssignable)
		FOnDead OnDead;

	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
			AActor* otherActor,
			UPrimitiveComponent* otherComponent,
			int32 otherBodyIndex,
			bool bFromSweep,
			const FHitResult& sweepResult);

	UFUNCTION(BlueprintCallable)
		void LoadDataScores();

	UFUNCTION(BlueprintCallable)
		void SaveDataScores();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void AddBall();

	void ChangeSpeed(float speedCoef);

	void ChangeSize(float sizeCoef);

	void ChangeBallsSpeed(float speedCoef);

	void AddBlowUpBall();

	void AddIgnoreBall(int count = 1);

	void AddIngnoreTimeBall(int count = 100);

	UFUNCTION(BlueprintCallable)
	void BallsStop();
};
