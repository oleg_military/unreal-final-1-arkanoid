// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusIgnoreBlock.h"
#include "PlayerPawn.h"

void ABonusIgnoreBlock::GiveBonus(AActor* bonusTarget)
{
	APlayerPawn* pawn = Cast<APlayerPawn>(bonusTarget);
	if (pawn) {
		pawn->AddIgnoreBall();
	}
}
