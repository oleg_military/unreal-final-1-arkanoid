// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockActor.h"
#include "GameFieldACtor.h"
#include "BonusBasic.h"
#include <Engine/Classes/Components/StaticMeshComponent.h>
#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>
#include <Math/UnrealMathUtility.h>


// Sets default values
ABlockActor::ABlockActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	blockMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("blockMeshComponent"));
	blockMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	blockMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	blockMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABlockActor::HandleBeginOverlap);

}

void ABlockActor::HandleBeginOverlap(UPrimitiveComponent* overlapComponent, AActor* otherActor, UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	IInteractorInterface* reflection = Cast<IInteractorInterface>(otherActor);
	if (reflection) {
		reflection->Interact(sweepResult);
	}
}

// Called when the game starts or when spawned
void ABlockActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockActor::Interact(AActor* otherActor, const FHitResult& hitResult)
{
	IInteractorInterface* reflection = Cast<IInteractorInterface>(otherActor);
	if (reflection) {
		reflection->Interact(this, hitResult);
	}

	HP -= BallKick;

	if (HP <= 0)
	{
		int v = FMath::RandRange(0, 100);

		if (bonusPrec >= v)
		{
			int type = FMath::RandRange(0, fieldOwner->mapBonusClass.Num() - 1);
			auto typeClass = fieldOwner->mapBonusClass[type];
			ABonusBasic* bonus = GetWorld()->SpawnActor<ABonusBasic>(typeClass, this->GetTransform());
		}

		if (IsValid(DeadSound))
			UGameplayStatics::PlaySoundAtLocation(this, DeadSound, GetActorLocation());
		
		Parent->fieldBlocks.Remove(this);
		this->Destroy();
	}
	else
	{
		if(IsValid(BlockMaterialDestruction))
			blockMeshComponent->SetMaterial(0, BlockMaterialDestruction);

		if (IsValid(HitBallSound))
			UGameplayStatics::PlaySoundAtLocation(this, HitBallSound, GetActorLocation());
	}
}

