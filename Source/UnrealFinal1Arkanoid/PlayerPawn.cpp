// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include <Engine/Classes/Components/CapsuleComponent.h>
#include <GameFramework/FloatingPawnMovement.h>
#include "GameFramework/PawnMovementComponent.h"
#include "BallActor.h"
#include "DataScore.h"
#include "BonusInterface.h"
#include <Kismet/GameplayStatics.h>


// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PlatformMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("platformMeshComponent"));
	PlatformMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PlatformMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	PlatformMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &APlayerPawn::HandleBeginOverlap);
	
	BallsSpawnCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("ballsSpawnCapsule"));
	BallsSpawnCapsule->AttachTo(PlatformMeshComponent);

	MovementComponent = CreateDefaultSubobject<UPawnMovementComponent, UFloatingPawnMovement>(TEXT("movementComponentName"));
	MovementComponent->UpdatedComponent = PlatformMeshComponent;

	Balls = TArray<ABallActor*>();

	DeltaMovement = FVector(0, 0, 0);
}

void APlayerPawn::HandlerPlayerPawnMoveInpput(float value)
{
	MovementSide = value;
}

//Input (Space)
void APlayerPawn::HandlerPlayerPushBallInpput()
{
	if (BallForSpawn != NULL)
	{
		BallForSpawn->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		BallForSpawn->speedBall = BallsSpeed;
		BallForSpawn->Movemed = true;
		BallForSpawn->pawnOwner = this;
		BallForSpawn = NULL;
	}
}

void APlayerPawn::HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
									AActor* otherActor,
									UPrimitiveComponent* otherComponent,
									int32 otherBodyIndex,
									bool bFromSweep,
									const FHitResult& sweepResult)
{
	UGameplayStatics::PlaySoundAtLocation(this, HitBallSound, GetActorLocation());

	IBonusInterface* bonus = Cast<IBonusInterface>(otherActor);
	if (bonus) {
		bonus->GiveBonus(this);
		otherActor->Destroy();

		UGameplayStatics::PlaySoundAtLocation(this, BonusSound, GetActorLocation());

		return;
	}

	IInteractorInterface* reflection = Cast<IInteractorInterface>(otherActor);
	if (reflection) {
		reflection->Interact(this, sweepResult);

		return;
	}
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	AddBall();

	if (OnKickPlayerLives.IsBound())
		OnKickPlayerLives.Broadcast();

	LoadDataScores();
}

void APlayerPawn::LoadDataScores()
{
	PlayerRecordScore = TArray<FDataScoreItem>();

	if (UGameplayStatics::DoesSaveGameExist("DataScore", 0))
	{
		DataScores = Cast<UDataScore>(UGameplayStatics::LoadGameFromSlot("DataScore", 0));

		if (DataScores != NULL)
		{
			PlayerRecordScore = DataScores->PlayerScore;
		}
		else
		{
			DataScores = Cast<UDataScore>(UGameplayStatics::CreateSaveGameObject(UDataScore::StaticClass()));

			if (DataScores != nullptr)
				UGameplayStatics::SaveGameToSlot(DataScores, "DataScore", 0);

			FDataScoreItem x = FDataScoreItem();
			x.Name = "null";
			x.Score = 0;

			PlayerRecordScore = TArray<FDataScoreItem>();
			PlayerRecordScore.Add(x);
		}

	}
	else
	{
		DataScores = Cast<UDataScore>(UGameplayStatics::CreateSaveGameObject(UDataScore::StaticClass()));

		if (DataScores != nullptr)
		{
			UGameplayStatics::SaveGameToSlot(DataScores, "DataScore", 0);
		}
	}
}

void APlayerPawn::SaveDataScores()
{
	while (PlayerRecordScore.Num() > 10)
		PlayerRecordScore.Pop();

	FDataScoreItem Item = FDataScoreItem();
	Item.Name = PlayerName;
	Item.Score = PlayerScore;

	PlayerRecordScore.Add(Item);
	PlayerRecordScore.Sort([](const FDataScoreItem& Lhs, const FDataScoreItem& Rhs) -> bool {return Lhs.Score > Rhs.Score; });

	DataScores->PlayerScore = PlayerRecordScore;

	UGameplayStatics::SaveGameToSlot(DataScores, "DataScore", 0);
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//IsValid(Balls)

	if (BallForSpawn == NULL && BallReserverCount > 0)
	{
		AddBall();
	}

	// Is ball null
	if (Balls.Num() == 0 && BallReserverCount == 0 && bDoOnce)
	{
		// Kick lives
		PlayerLives--;

		if (OnKickPlayerLives.IsBound())
			OnKickPlayerLives.Broadcast();

		if (PlayerLives <= 0)
		{
			
			BallsStop();

			// SaveDataScores();
			
			// UGameplayStatics::SaveGameToSlot(DataScores, "DataScore", 0);

			if (OnDead.IsBound())
				OnDead.Broadcast();

			bDoOnce = false;
		}
		else
		{
			BallReserverCount++;

			UGameplayStatics::PlaySoundAtLocation(this, HitBallDead, GetActorLocation());
		}
	}

	DeltaMovement.Y = (DeltaMovement.Y - DeltaMovement.Y * 0.5) + MovementSide * DeltaTime * MovementSpeed * MovementCoef;
	AddActorWorldOffset(DeltaMovement, true);
}



// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("PawnMove", this, &APlayerPawn::HandlerPlayerPawnMoveInpput);
	PlayerInputComponent->BindAction("PushBall", IE_Pressed, this, &APlayerPawn::HandlerPlayerPushBallInpput);
}

void APlayerPawn::AddBall()
{
	if (BallForSpawn == NULL && BallReserverCount > 0)
	{
		FTransform ballTransfrom = BallsSpawnCapsule->GetComponentTransform();
		ballTransfrom.SetScale3D(FVector(1, 1, 1));

		BallForSpawn = GetWorld()->SpawnActor<ABallActor>(BallActorClass, ballTransfrom);
		BallForSpawn->AttachToComponent(BallsSpawnCapsule, FAttachmentTransformRules::KeepWorldTransform);
		BallForSpawn->indexBall = Balls.Add(BallForSpawn);
		BallForSpawn->speedBall = 0;
		BallForSpawn->Movemed = false;

		BallReserverCount--;
	}
}


// Pawn Speed
void APlayerPawn::ChangeSpeed(float speedCoef)
{
	MovementCoef += speedCoef;
	MovementCoef = FMath::Max(MovementCoef, 0.1f);
	MovementCoef = FMath::Min(MovementCoef, 2.f);	
}

// Pawn Size
void APlayerPawn::ChangeSize(float sizeCoef)
{
	PawnScale += sizeCoef;
	PawnScale = FMath::Max<float>(PawnScale, 0.5);
	PawnScale = FMath::Min<float>(PawnScale, 2);
	PlatformMeshComponent->SetWorldScale3D(FVector(1, PawnScale, 1));
	BallsSpawnCapsule->SetWorldScale3D(FVector(1,1,1));
}

// Ball Speed
void APlayerPawn::ChangeBallsSpeed(float speedCoef)
{
	BallsSpeed += speedCoef;
	BallsSpeed = FMath::Max<float>(BallsSpeed, 0.4);
	BallsSpeed = FMath::Min<float>(BallsSpeed, 3);

	for (auto ball = Balls.CreateIterator(); ball; ball++) {
		(*ball)->speedBall = BallsSpeed;
	}
}

void APlayerPawn::AddBlowUpBall()
{
	int n = FMath::RandRange(0, Balls.Num() - 1);
	
	if (Balls.IsValidIndex(n))
	{
		Balls[n]->ballState = EBallState::BLOWUP;
		Balls[n]->ballMeshComponent->SetMaterial(0, Balls[n]->blowupMaterial);
	}
}

void APlayerPawn::AddIgnoreBall(int count)
{
	int n = FMath::RandRange(0, Balls.Num() - 1);

	if (Balls.IsValidIndex(n))
	{
		Balls[n]->ballState = EBallState::IGNOREONE;
		Balls[n]->ignoreValue = count;
		Balls[n]->ballMeshComponent->SetMaterial(0, Balls[n]->ignoreMaterial);
	}
}

void APlayerPawn::AddIngnoreTimeBall(int count)
{
	int n = FMath::RandRange(0, Balls.Num() - 1);

	if (Balls.IsValidIndex(n))
	{
		Balls[n]->ballState = EBallState::IGNOREONE;
		Balls[n]->ignoreValue = count;
		Balls[n]->ballMeshComponent->SetMaterial(0, Balls[n]->ignoreMaterial);
	}
}

void APlayerPawn::BallsStop()
{
	for (int i = 0; i < Balls.Num(); i++)
		Balls[i]->speedBall = 0;
}