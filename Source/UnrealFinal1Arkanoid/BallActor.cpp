// Fill out your copyright notice in the Description page of Project Settings.


#include "BallActor.h"
#include "PlayerPawn.h"
#include "BlowupActor.h"
#include "BlockActor.h"
#include <Engine/Classes/Components/StaticMeshComponent.h>
#include <GameFramework/KillZVolume.h>

// Sets default values
ABallActor::ABallActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ballMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ballMeshComponent"));
	ballMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ballMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	ballMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ABallActor::HandleBeginOverlap);

	Movemed = false;
}

void ABallActor::HandleBeginOverlap(UPrimitiveComponent* overlapComponent, AActor* otherActor, UPrimitiveComponent* otherComponent, int32 otherBodyIndex, bool bFromSweep, const FHitResult& sweepResult)
{
	IInteractorInterface* reflection = Cast<IInteractorInterface>(otherActor);
	if (reflection) {
		reflection->Interact(this, sweepResult);
	}
}

// Called when the game starts or when spawned
void ABallActor::BeginPlay()
{
	Super::BeginPlay();
	FVector currentPosition = GetActorLocation();
	movementDiraction.Y = FMath::RandRange(-5, 5);
	movementDiraction.X = 5;
	movementDiraction.Z = 0;

}

// Called every frame
void ABallActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Movemed)
	{
		movementDiraction.Z = 0;
		AddActorWorldOffset(movementDiraction * speedBall, true, hitResult);

		if (currentBallState == EBallState::IGNORETIME) {
			if (ignoreValue == 0) {
				currentBallState = EBallState::STANDART;
			}
			else {
				ignoreValue--;
			}
		}
	}
}

void ABallActor::Interact(AActor* otherActor, const FHitResult& hit)
{
	ABallActor* ball = Cast <ABallActor>(otherActor);

	if (ball)
		return;
	
	ABlockActor* tergetBlock = Cast<ABlockActor>(otherActor);
	if (tergetBlock) 
	{
		switch (ballState)
		{
			case EBallState::STANDART:
				movementDiraction = FMath::GetReflectionVector(movementDiraction, hit.Normal);
				break;

			case EBallState::IGNOREONE:
				if (--ignoreValue == 0) {
					ballState = EBallState::STANDART;
					ballMeshComponent->SetMaterial(0, standrtMaterial);
				}
				break;

			case EBallState::IGNORETIME:
				if (ignoreValue == 0) {
					ballState = EBallState::STANDART;
					ballMeshComponent->SetMaterial(0, standrtMaterial);
				}
				break;

			case EBallState::BLOWUP:
				ballMeshComponent->SetMaterial(0, standrtMaterial);
				movementDiraction = FMath::GetReflectionVector(movementDiraction, hit.Normal);
				ballState = EBallState::STANDART;
				break;

			default:
				movementDiraction = FMath::GetReflectionVector(movementDiraction, hit.Normal);
				break;
		}

		pawnOwner->PlayerScore += tergetBlock->blockScore;

		return;
	}

	movementDiraction = FMath::GetReflectionVector(movementDiraction, hit.Normal);
}





