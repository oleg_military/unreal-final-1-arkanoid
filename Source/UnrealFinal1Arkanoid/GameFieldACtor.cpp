// Fill out your copyright notice in the Description page of Project Settings.


#include "GameFieldACtor.h"
#include "BlockActor.h"
#include "Components/BoxComponent.h"



// Sets default values
AGameFieldACtor::AGameFieldACtor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	spawnField = CreateDefaultSubobject<UBoxComponent>(TEXT("spawndField"));
	spawnField->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

// Called when the game starts or when spawned
void AGameFieldACtor::BeginPlay()
{
	Super::BeginPlay();
	
	//SpawnXTypeField();
}

// Called every frame
void AGameFieldACtor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGameFieldACtor::SpawnXTypeField()
{
	// Get Vector point: min, max 
	CubePointMin = spawnField->GetNavigationBounds().Min;
	CubePointMax = spawnField->GetNavigationBounds().Max;
	CubeSizeMax = spawnField->GetNavigationBounds().GetSize();

	// Middle Vector point
	FVector* midPoint = new FVector();
	midPoint->X = (CubePointMin.X + CubePointMax.X) / 2;
	midPoint->Y = (CubePointMin.Y + CubePointMax.Y) / 2;
	midPoint->Z = (CubePointMin.Z + CubePointMax.Z) / 2;

	// Set middle spawn Vector point
	FVector* spawnedBlockPossition = new FVector();
	*spawnedBlockPossition = *midPoint;

	// Block: Transform
	FTransform blockTransform = FTransform();
	blockTransform.SetLocation(*spawnedBlockPossition);
	blockTransform.SetScale3D(FVector(1, 1, 1));
	
	if (blocksActorClass.Num() > 0)
	{
		float blocksActorClassID;

		fieldBlocks = TArray<ABlockActor*>();

		FVector* blockMidPoint = new FVector();
		blockMidPoint->X = (CubeSizeMax.X / CountX);
		blockMidPoint->Y = (CubeSizeMax.Y / CountY);

		for (int PossitionY = 0; PossitionY < CountY; PossitionY++)
		{
			for (int PossitionX = 0; PossitionX < CountX; PossitionX++)
			{
				if (DrawX(PossitionX, PossitionY, CountX, CountY, LevelType))
				{
					blocksActorClassID = FMath::RandRange(0, blocksActorClass.Num() - 1);

					spawnedBlockPossition->X = CubePointMin.X + PossitionX * blockMidPoint->X + blockMidPoint->X / 2;
					spawnedBlockPossition->Y = CubePointMin.Y + PossitionY * blockMidPoint->Y + blockMidPoint->Y / 2;

					blockTransform.SetLocation(*spawnedBlockPossition);
					ABlockActor* newBlock = GetWorld()->SpawnActor<ABlockActor>(blocksActorClass[blocksActorClassID], blockTransform);
					
					newBlock->Parent = this;

					if (IsValid(newBlock))
					{
						newBlock->fieldOwner = this;
					}	

					fieldBlocks.Add(newBlock);
				}
			}
		}
	}
}

bool AGameFieldACtor::DrawX(int X, int Y, int width, int height, int EFLevelType)
{
	if (EFLevelType == 0)
	{
		if (X % 2 == Y % 2)
			return true;
	}

	if (EFLevelType == 1)
	{
		if (width < height)
		{
			if (X == Y || X == width - Y - 1)
				return true;
		}
		else
		{
			if (X == Y || Y == height - X - 1)
				return true;
		}
	}

	return false;
}

int AGameFieldACtor::Count()
{
	return fieldBlocks.Num();
}

void AGameFieldACtor::AllKill()
{
	while (fieldBlocks.Num())
	{
		for (int i = 0; i < fieldBlocks.Num(); i++)
		{
			if (fieldBlocks.IsValidIndex(i))
			{
				fieldBlocks[i]->Destroy();
				fieldBlocks.RemoveAt(i);
			}
		}
	}
}

