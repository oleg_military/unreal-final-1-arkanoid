// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusBlowUp.h"
#include "PlayerPawn.h"

void ABonusBlowUp::GiveBonus(AActor* bonusTarget)
{
	APlayerPawn* pawn = Cast<APlayerPawn>(bonusTarget);
	if (pawn) {
		pawn->AddBlowUpBall();
	}
}
