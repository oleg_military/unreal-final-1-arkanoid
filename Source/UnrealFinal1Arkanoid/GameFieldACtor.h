// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GameFieldACtor.generated.h"

class UBoxComponent;
class ABlockActor;
class ABonusBasic;

/*
	UENUM(BlueprintType)
	enum class EFieldLevelType : uint8
	{
		X,
		CHESS
	};
*/

UCLASS()
class UNREALFINAL1ARKANOID_API AGameFieldACtor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameFieldACtor();

	UPROPERTY(BlueprintReadWrite)
		int LevelType;

	UPROPERTY()
		FVector CubePointMin;

	UPROPERTY()
		FVector CubePointMax;

	UPROPERTY()
		FVector CubeSizeMax;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UBoxComponent* spawnField;

	UPROPERTY(EditDefaultsOnly)
		int CountX;

	UPROPERTY(EditDefaultsOnly)
		int CountY;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TArray<ABlockActor*> fieldBlocks;

	UPROPERTY(EditDefaultsOnly)
		TArray<TSubclassOf<ABlockActor>> blocksActorClass;

	UPROPERTY(EditDefaultsOnly)
		TMap<int, TSubclassOf<ABonusBasic>> mapBonusClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void SpawnXTypeField();

	UFUNCTION(BlueprintCallable)
		int Count();

	UFUNCTION(BlueprintCallable)
		void AllKill();

private:
	UFUNCTION()
	bool DrawX(int X, int Y, int width, int height, int EFLevelType);
};
