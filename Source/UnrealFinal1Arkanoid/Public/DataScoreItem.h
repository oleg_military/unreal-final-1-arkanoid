// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DataScoreItem.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct FDataScoreItem
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Data")
		int32 Score = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Data")
		FString Name = "No Name";
};
