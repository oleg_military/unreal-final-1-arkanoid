// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Public/DataScoreItem.h"
#include "DataScore.generated.h"

/**
 * 
 */
UCLASS()
class UNREALFINAL1ARKANOID_API UDataScore : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		TArray<FDataScoreItem> PlayerScore;

		UDataScore();
};
