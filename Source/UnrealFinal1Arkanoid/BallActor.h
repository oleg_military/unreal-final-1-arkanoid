// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractorInterface.h"
#include "BallActor.generated.h"


class UStaticMeshComponent;
class APlayerPawn;
class ABlowupActor;

UENUM()
enum class EBallState {
	STANDART, 
	IGNOREONE,
	IGNORETIME,
	BLOWUP
};

UCLASS()
class UNREALFINAL1ARKANOID_API ABallActor : public AActor, public IInteractorInterface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABallActor();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		int indexBall;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		FVector movementDiraction;

	UPROPERTY(BlueprintReadWrite)
		float speedBall = 1;

	UPROPERTY(EditDefaultsOnly)
		float blowupRadius;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		UStaticMeshComponent* ballMeshComponent;

	UPROPERTY(EditDefaultsOnly)
		UMaterial* standrtMaterial;

	UPROPERTY(EditDefaultsOnly)
		UMaterial* ignoreMaterial;

	UPROPERTY(EditDefaultsOnly)
		UMaterial* blowupMaterial;

	UPROPERTY(VisibleAnyWhere, BlueprintReadWrite)
		EBallState ballState = EBallState::STANDART;

	UPROPERTY(BlueprintReadOnly)
		APlayerPawn* pawnOwner;

	UPROPERTY(BlueprintReadOnly)
		bool Movemed;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABlowupActor> blowupActor;



	UFUNCTION()
		void HandleBeginOverlap(UPrimitiveComponent* overlapComponent,
			AActor* otherActor,
			UPrimitiveComponent* otherComponent,
			int32 otherBodyIndex,
			bool bFromSweep,
			const FHitResult& sweepResult);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//void ChangeMaterial(int effectType);

	EBallState currentBallState = EBallState::STANDART;

	int ignoreValue;

	FHitResult* hitResult = new FHitResult();

	virtual void Interact(AActor* otherActor, const FHitResult& hitResult) override;

};
