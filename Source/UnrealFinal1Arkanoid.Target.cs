// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class UnrealFinal1ArkanoidTarget : TargetRules
{
	public UnrealFinal1ArkanoidTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "UnrealFinal1Arkanoid" } );
	}
}
