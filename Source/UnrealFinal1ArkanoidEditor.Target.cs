// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class UnrealFinal1ArkanoidEditorTarget : TargetRules
{
	public UnrealFinal1ArkanoidEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "UnrealFinal1Arkanoid" } );
	}
}
